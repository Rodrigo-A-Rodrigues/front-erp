/// <reference types='vitest' />

import { defineConfig } from 'vite';
import tsconfigPaths from 'vite-tsconfig-paths'
import react from '@vitejs/plugin-react'

export default defineConfig({
    test: {
        environment: 'jsdom',
        include: ["./**/*.test.ts", "./**/*.test.tsx"],
        globals: true,
        setupFiles: ['./setupTests.js'],
        reporters: ['default', 'html'],
        coverage: {
            reporter: ['text', 'html', 'lcov'],
            enabled: true,
            exclude: [
                '.eslintrc.json',
                '.babelrc',
                '.gitignore',
                '.prettierrc.json',
                '.vscode/**',
                'coverage/**',
                'node_modules/**',
                '__tests__/**',
                'helm/**',
                'public/**',
                '.next/**',
                'Dockerfile',
                'README.md',
                'vite.config.mts',
                'next.config.mjs',
                'next-env.d.ts',
                'package.json',
                'package-lock.json',
                '.editorconfig',
                '.env.*',
                'src/styles/**',
                'src/attrs/**',
                'src/context/**',
                'src/pages/page.tsx',
                'src/app/layout.tsx',
                'src/types/**',
            ],
        },
    },
    plugins: [tsconfigPaths(), react()],
})

// Para exibir a árvore do DOM no console
// console.debug(prettyDOM())