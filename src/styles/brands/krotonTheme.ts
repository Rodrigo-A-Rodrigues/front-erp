import { extendTheme } from '@chakra-ui/react';
import { styles } from '../global/commonTokens';

const brandColors = {
  colors: {
    base: {
      tenth: 'rgba(0, 31, 80, 1)',
      ninth: 'rgba(0, 40, 113, 1)',
      eighth: 'rgba(0, 52, 145, 1)',
      seventh: 'rgba(0, 65, 175, 1)',
      sixth: 'rgba(0, 78, 204, 1)',
      fifth: 'rgba(57, 105, 235, 1)',
      fourth: 'rgba(73, 142, 255, 1)',
      third: 'rgba(115, 137, 255, 1)',
      second: 'rgba(172, 177, 255, 1)',
      primary: 'rgba(229, 237, 250, 1)',
    }
  }
};

const krotonTheme = extendTheme(brandColors, styles);

export default krotonTheme;
