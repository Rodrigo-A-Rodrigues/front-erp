import { extendTheme } from '@chakra-ui/react';
import { styles } from '../global/commonTokens';

const brandColors = {
  colors: {
    base: {
      tenth: 'rgba(0, 50, 89, 1)',
      ninth: 'rgba(0, 54, 89, 1)',
      eighth: 'rgba(0, 75, 108, 1)',
      seventh: 'rgba(0, 104, 134, 1)',
      sixth: 'rgba(0, 138, 160, 1)',
      fifth: 'rgba(1, 177, 187, 1)',
      fourth: 'rgba(54, 214, 209, 1)',
      third: 'rgba(94, 234, 218, 1)',
      second: 'rgba(149, 248, 228, 1)',
      primary: 'rgba(201, 251, 237, 1)',
    }
  }
};

const haocTheme = extendTheme(brandColors, styles);

export default haocTheme;
