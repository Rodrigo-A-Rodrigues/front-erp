import { extendTheme } from '@chakra-ui/react';
import { styles } from '../global/commonTokens';

const brandColors = {
  colors: {
    base: {
      tenth: 'rgba(0, 10, 45, 1)',
      ninth: 'rgba(0, 13, 55, 1)',
      eighth: 'rgba(0, 19, 66, 1)',
      seventh: 'rgba(1, 27, 82, 1)',
      sixth: 'rgba(2, 37, 98, 1)',
      fifth: 'rgba(3, 48, 115, 1)',
      fourth: 'rgba(46, 101, 171, 1)',
      third: 'rgba(88, 148, 213, 1)',
      second: 'rgba(147, 196, 240, 1)',
      primary: 'rgba(199, 226, 248, 1)'
    }
  }
};

const umcTheme = extendTheme(brandColors, styles);

export default umcTheme;
