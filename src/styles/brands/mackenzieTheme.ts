import { extendTheme } from '@chakra-ui/react';
import { styles } from '../global/commonTokens';

const brandColors = {
  colors: {
    gray: {
      450: 'rgba(177, 194, 213, 1)',
    },
    base: {
      tenth: 'rgba(115, 10, 64, 1)',
      ninth: 'rgba(108, 0, 54, 1)',
      eighth: 'rgba(130, 0, 53, 1)',
      seventh: 'rgba(162, 0, 51, 1)',
      sixth: 'rgba(193, 0, 43, 1)',
      fifth: 'rgba(225, 0, 30, 1)',
      fourth: 'rgba(236, 59, 66, 1)',
      third: 'rgba(246, 106, 98, 1)',
      second: 'rgba(252, 165, 151, 1)',
      primary: 'rgba(253, 214, 202, 1)',
    },
    red: {
      650: 'rgba(162, 0, 68, 1)',
    },
    blue: {
      900: 'rgba(44, 64, 83, 1)',
    }
  }
};

const mackenzieTheme = extendTheme(brandColors, styles);

export default mackenzieTheme;
