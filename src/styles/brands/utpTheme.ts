import { extendTheme } from '@chakra-ui/react';
import { styles } from '../global/commonTokens';

const brandColors = {
  colors: {
    base: {
      tenth: 'rgba(8, 65, 74, 1)',
      ninth: 'rgba(9, 70, 74, 1)',
      eighth: 'rgba(15, 89, 87, 1)',
      seventh: 'rgba(24, 111, 100, 1)',
      sixth: 'rgba(35, 133, 110, 1)',
      fifth: 'rgba(48, 155, 120, 1)',
      fourth: 'rgba(94, 195, 152, 1)',
      third: 'rgba(131, 225, 176, 1)',
      second: 'rgba(177, 245, 202, 1)',
      primary: 'rgba(215, 250, 225, 1)'
    }
  }
};

const utpTheme = extendTheme(brandColors, styles);

export default utpTheme;
