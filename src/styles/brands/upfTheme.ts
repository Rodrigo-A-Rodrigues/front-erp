import { extendTheme } from '@chakra-ui/react';
import { styles } from '../global/commonTokens';

const brandColors = {
  colors: {
    base: {
      tenth: 'rgba(5, 68, 8, 1)',
      ninth: 'rgba(7, 72, 12, 1)',
      eighth: 'rgba(13, 87, 13, 1)',
      seventh: 'rgba(27, 108, 20, 1)',
      sixth: 'rgba(44, 129, 29, 1)',
      fifth: 'rgba(65, 151, 41, 1)',
      fourth: 'rgba(118, 192, 87, 1)',
      third: 'rgba(161, 223, 125, 1)',
      second: 'rgba(204, 244, 173, 1)',
      primary: 'rgba(231, 249, 213, 1)'
    }
  }
};

const upfTheme = extendTheme(brandColors, styles);

export default upfTheme;
