export const commonTokens = {
  sizes: {
    '0': '0px',
    '1': '4px',
    '2': '8px',
    '3': '12px',
    '4': '16px',
    '5': '20px',
    '6': '24px',
    '7': '28px',
    '8': '32px',
    '9': '36px',
    '10': '40px',
    '11': '44px',
    '12': '48px',
    '13': '52px',
    '14': '56px',
    '16': '64px',
    '18': '72px',
    '20': '80px',
    '24': '96px',
    '32': '128px',
    '025': '1px',
    '050': '2px'
  },
  radii: {
    none: '0px',
    xs: '4px',
    sm: '8px',
    md: '12px',
    lg: '16px',
    ul: '24px',
    pill: '500px',
    circle: '50%',
    main: '12px',
    variantSmaller: '8px',
    variantLarger: '16px'
  },
  borderWidths: { none: '0px', sm: '1px', md: '2px', lg: '4px' },
  shadows: {
    1: '2px 2px 2px 0px rgba(0,0,0,0.14)',
    2: '4px 4px 4px 0px rgba(0,0,0,0.14)',
    3: 'inset 6px 6px 6px 0px rgba(0,0,0,0.14)',
    4: 'inset 8px 8px 8px 0px rgba(0,0,0,0.14)',
    5: '12px 12px 12px 0px rgba(0,0,0,0.14)',
    6: '16px 16px 16px 0px rgba(0,0,0,0.14)'
  },
  fonts:{
    body:'Roboto',
    heading:'Roboto',
    mono:'Roboto'
  },
  fontSizes:{
    xs:'0.75rem',
    xs2:'0.875rem',
    xs3:'1rem',
    sm:'1.25rem',
    md:'1.5rem',
    lg:'2rem',
    xl:'2.5rem',
    xl2:'3rem',
    xl3:'4rem',
    ul1:'4.5rem',
    ul2:'5rem',
    ul3:'6rem',
    ul4:'10rem'
  },
  lineHeights:{
    xs:'115%',
    sm:'120%',
    md:'140%',
    lg:'148%',
    xl:'168%'
  },
  fontWeights:{
    light:'300',
    regular:'400',
    medium:'500',
    semibold:'600',
    bold:'700',
    black:'900'
  }
}

export const styles = {
  global: {
    body: {
      bg: 'gray.100',
      color: 'gray.900',
    }
  },
}
