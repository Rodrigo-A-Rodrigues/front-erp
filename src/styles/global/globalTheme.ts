import { extendTheme, type ThemeConfig } from "@chakra-ui/react";
import { darkColors } from "./darkColors";
import { lightColors } from "./lightColors";
import { commonTokens, styles } from './commonTokens';
import { textStyles } from "./textStyles";


const colorMode: ThemeConfig = {
  initialColorMode: 'light',
  useSystemColorMode: true,
}

const lightTheme = extendTheme({
  ...commonTokens,
  colors: lightColors,
  colorMode,
  styles,
  textStyles
})

const darkTheme = extendTheme({
  ...commonTokens,
  colors: darkColors,
  styles,
  textStyles,
})

export { lightTheme, darkTheme };
