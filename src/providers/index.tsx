'use client'

import { styles } from '@/styles/global/commonTokens'
import { ChakraProvider } from '@chakra-ui/react'


interface ChakraProviderProps {
  children: React.ReactNode
}

export function Providers({ children }: Readonly<ChakraProviderProps>) {
  return <ChakraProvider theme={styles}>{children}</ChakraProvider>
}
